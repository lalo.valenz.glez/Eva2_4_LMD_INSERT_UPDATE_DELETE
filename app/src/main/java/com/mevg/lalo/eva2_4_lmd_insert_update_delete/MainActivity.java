package com.mevg.lalo.eva2_4_lmd_insert_update_delete;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = openOrCreateDatabase("app",MODE_PRIVATE, null);

        db.execSQL("create table if not exists data (" +
                "id integer primary key autoincrement," +
                "name text," +
                "phone text) ");
        ContentValues values = new ContentValues();
        long idRow = 0;
        values.put("name", "lalo");
        values.put("phone", "111-222-333");
        idRow = db.insert("data",null, values);
        Toast.makeText(MainActivity.this, "idRow: " + idRow, Toast.LENGTH_SHORT).show();
        values.put("name", "ana");
        values.put("phone", "111-222-333");
        idRow = db.insert("data",null, values);
        Toast.makeText(MainActivity.this, "idRow: " + idRow, Toast.LENGTH_SHORT).show();
        values.put("name", "alan");
        values.put("phone", "111-222-333");
        idRow = db.insert("data",null, values);
        Toast.makeText(MainActivity.this, "idRow: " + idRow, Toast.LENGTH_SHORT).show();
        values.put("name", "majo");
        values.put("phone", "111-222-333");
        idRow = db.insert("data",null, values);
        Toast.makeText(MainActivity.this, "idRow: " + idRow, Toast.LENGTH_SHORT).show();
        values.clear();
        idRow = db.insert("data",null, values);
        Toast.makeText(MainActivity.this, "idRow: " + idRow, Toast.LENGTH_SHORT).show();
        values.put("name","nulo");
        idRow = db.insert("data","phone", values);
        Toast.makeText(MainActivity.this, "idRow: " + idRow, Toast.LENGTH_SHORT).show();
        //update
        ContentValues data = new ContentValues();
        long upRow;
        data.put("name","Eduardo");
        upRow = db.update("data",data,"id = ?",new String[]{"5"});
        Toast.makeText(MainActivity.this, "upRown: " + upRow, Toast.LENGTH_SHORT).show();
    }
}
